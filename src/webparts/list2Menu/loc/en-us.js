define([], function() {
  return {
    "PropertyPaneDescription": "Configuration",
    "BasicGroupName": "Settings",
    "TitleFieldLabel": "Title",
    "ListNameFieldLabel": "Menu"
  }
});