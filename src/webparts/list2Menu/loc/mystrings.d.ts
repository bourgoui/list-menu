declare interface IList2MenuWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  TitleFieldLabel: string;
  ListNameFieldLabel: string;
}

declare module 'List2MenuWebPartStrings' {
  const strings: IList2MenuWebPartStrings;
  export = strings;
}
