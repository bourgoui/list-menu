import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneDropdown,
  IPropertyPaneDropdownOption
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './List2MenuWebPart.module.scss';
import * as strings from 'List2MenuWebPartStrings';

import {
  SPHttpClient,
  SPHttpClientResponse,
  ISPHttpClientOptions
 } from '@microsoft/sp-http';

 import {
  Environment,
  EnvironmentType
 } from '@microsoft/sp-core-library';

 export interface ISPLists {
  value: ISPList[];
 }
 
 export interface ISPList {
  Title: string;
  Id: string;
 }

export interface IList2MenuWebPartProps {
  title: string;
  listName: string;
}

const linksList: string = "/sites/intranet/Lists/TOOLS";

export default class List2MenuWebPart extends BaseClientSideWebPart<IList2MenuWebPartProps> {
  
  private _menuOptions: IPropertyPaneDropdownOption[] = [];

  // Gets the menu options from SP
  private _getMenuOptions(): void {
    if (Environment.type == EnvironmentType.SharePoint || Environment.type == EnvironmentType.ClassicSharePoint) {
      this._getMenuFolders()
        .then((response) => {
          //this._renderList(response.value);
		      //console.log(response);
          if (response) {
            this._menuOptions = response.Row.map((menu) => {
              return {
                key: menu.FileRef,
                text: menu.FileLeafRef
              };   
            });
          }
          //console.log(this._menuOptions);
        });
    } else {
      alert('Please use a real SharePoint Environment');
    }
  }

  // Builds the menu links
  private _renderMenu() {
    let html: string = '';
    const listContainer: Element = this.domElement.querySelector('#spMenuItems');
    if (this.properties.listName) {
      this._getMenuItems().then((response) => {
        //console.log(response);
        // Loop over Row arrays for link items
        response.Row.forEach((linkItem) => {
          let target = (linkItem['New_x0020_Window'] == "Yes") ? `target="_blank"`:``;
          html += `
          <li>
            <a href="${linkItem['URL']}" ${target}>${escape(linkItem['URL.desc'])}</a>
          </li>`;
        });
        // Display        
        listContainer.innerHTML = html;
      });
    } else {
      listContainer.innerHTML = `<h3>Please configure the menu</h3>`;
    }
  }

  // Get the selected list items
  private _getMenuItems():Promise<any> {
    const spOpts: ISPHttpClientOptions = {
      headers: {
        "Accept": "application/json;odata=verbose",
        "Content-Type": "application/json;odata=verbose",
      },
      body: JSON.stringify({
        parameters: {
          ViewXml: `
            <View>
              <ViewFields>
                <FieldRef Name="ID" />
                <FieldRef Name="URL" />
                <FieldRef Name="New_x0020_Window" />
              </ViewFields>
              <Query />
              <RowLimit />
            </View>
          `,
          FolderServerRelativeUrl: this.properties.listName
        }
      })
    };
    return this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl +
      `/_api/web/GetList('${encodeURIComponent(linksList)}')/RenderListDataAsStream?SortField=SortOrder`, SPHttpClient.configurations.v1, spOpts).
    then((response:SPHttpClientResponse)=>{
      return response.json();
    }); 
  }
  
  private _getMenuFolders(): Promise<any> {
    const spOpts: ISPHttpClientOptions = {
      headers: {
        "Accept": "application/json;odata=verbose",
        "Content-Type": "application/json;odata=verbose",
      },
      body: JSON.stringify({
        parameters: {
          ViewXml: `
            <View>
              <ViewFields>
                <FieldRef Name="ID" />
                <FieldRef Name="FileLeafRef" />
              </ViewFields>
              <Query />
              <RowLimit />
            </View>
          `
        }
      })
    };
    return this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl +
      `/_api/web/GetList(@listUrl)/RenderListDataAsStream?@listUrl='${encodeURIComponent(linksList)}'`, 
      SPHttpClient.configurations.v1, spOpts).then((response: SPHttpClientResponse) => {
        return response.json();
      });
  }

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.list2Menu }">
        <div class="${ styles.container }">
          <h1 class="${ styles.title }"> ${escape(this.properties.title)} </h1>
          <div class="${ styles.body }">
            <ul class="${ styles.links }" id="spMenuItems" />
          </div>
        </div>
      </div>`;

    // Populate Menu (called only once)
    if (this.renderedOnce === false)  this._getMenuOptions();
    
    // Render Selected Menu
    if (this.properties.listName) this._renderMenu();

  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('title', {
                  label: strings.TitleFieldLabel
                }),
                PropertyPaneDropdown("listName", {
                  label: strings.ListNameFieldLabel,
                  options: this._menuOptions,
                  selectedKey: 0,
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
